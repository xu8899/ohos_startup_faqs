# passwd/ group

- 文件路径：base\startup\init\services\etc\

- 文件内容：

  - passwd：每行记录对应一个用户，用户信息以":"作为分隔符，划分七个字段;如

    ```js
    root:x:0:0:::/bin/false
    bin:x:2:2:::/bin/false
    system:x:1000:1000:::/bin/false
    ```

  - group：每行记录对应一个用户组，用户组信息以":"作为分隔符，划分四个字段;如：

    ```js
    root:x:0:
    bin:x:2:
    system:x:1000:
    servicectrl:x:1050:root,shell,system,samgr,hdf_devmgr,foundation,update
    ```

- 字段含义

  | 字段      | 含义                                                         |
  | --------- | ------------------------------------------------------------ |
  | 用户名    | 方便用户记忆的代号，如root，文件种定义了用户名喝UID直接的对应关系，系统通过UID识别用户身份，分配用户权限 |
  | 密码      | "x"表示用户设有密码，不代表真正的密码,已被映射到/etc/shadow 文件中；"x"不可删除，null表示无密码系统把真正的加密码密码串放在/etc/shadow文件种，只有root用户可以浏览和操作，最大限度的保证密码的安全性 |
  | UID       | 每个用户有唯一的UID，系统通过UID识别用户身份<br />UID在0 ~ 65535之间的数，不同范围的数字代表不同的用户身份<br />1. 0-超级用户，代表管理员账号<br />2. 1~499代表系统用户<br />3. >=500代表普通用户 |
  | GID       | 表示初始组组ID号初始组：用户登陆时就拥有该用户组的相关权限，每个用户的初始组只有一个，通常是将此用户的用户名相同的组名作为该用户的初始组附加组：用户可以加入多个其他的用户组，并拥有这些组的权限；除初始组外，用户加入的其他组，这些用户组即为附加组 |
  | 描述信息  | 解释用户的意义                                               |
  | 主目录    | 用户的主目录，如root的主目录为/root                          |
  | 默认shell | shell是linux的命令解释器，用户和Linux内核之间沟通的桥梁shell命令解释器用于将用户输入的命令转换成系统可识别的机器语言通常情况下，Linux系统默认使用的命令解释器是/bin/bash |


# *.cfg

- 文件描述: 配置文件基于JSON格式，遵循JSON的基本规则；由import,jobs,services构成

  - import：表示依赖的cfg配置，格式语法一样，需要解析加载，一般与硬件设备相关的可以在此加载；以数组的形式放入需要import的cfg文件路径；对于import解析，在解析完成一个import中的cfg文件路径时，会立即解析该cfg文件

  - jobs：init组件下cfg文件中的一组命令构成的集合，最多4096个job；可以通过start\stop命令启动服务，通过trigger命令触发其他job;在init启动过程中执行，服务于service的正常启动或特定基础功能的初始化

    | 字段名    | 说明                                                         |
    | --------- | ------------------------------------------------------------ |
    | name      | 用户自定义；若条件是系统参数，以"param:"为前缀               |
    | cmds      | 包含于系统支持的命令                                         |
    | condition | 可选属性，通常是系统参数，可以指定某个系统参数被设定为某个值时该job触发执行(*表示只要该系统参数被设置，不管值为何，job即被触发) |

  - service: 定义的服务的名称、执行path、触发条件等，服务也可以通过调用jobs。

    | 字段名     | 说明                                                         |
    | ---------- | ------------------------------------------------------------ |
    | name       | 当前服务的名称(非空及长度<=32byte)                           |
    | path       | 当前服务的可执行文件路径和参数，以数组形式(第一个数组元素为可执行文件路径、数组元素个数≤20、每个元素为字符串形式以及每个字符串长度≤64字节) |
    | uid        | 当前服务进程的uid                                            |
    | gid        | 当前服务进程的gid                                            |
    | secon      | 当前服务进程的安全上下文,selinux标签                         |
    | once       | 当前服务进程是否为一次性进程：1-一次性进程，当进程退出时，init不会重新启动该服务进程0-常驻进程，当进程退出时，init收到SIGCHLD信号并重新启动服务进程 |
    | importance | 当前服务进程优先级(取值范围[19,-20])                         |
    | caps       | 当前服务进程的capability值，根据安全子系统已支持的capability评估其所需的capability，遵循最小权限原则配置(当前最多可以配置100个值) |
    | critical   | 服务启动失败后，需要M秒内重启拉起，拉起失败N后，直接重启系统(N默认为4， M默认20。（仅L2以上提供 "critical" : [0, 2, 10]）：使能0，执行次数2，执行时间10)0-不使能1-使能 |
    | cpucores   | 服务需要的绑定的cpu核心数，类型为init型数组                  |
    | d-caps     | 分布式能力(如OHOS_DMS)                                       |
    | apl        | 能力特权级别：system_care,normal,system_basic.默认system_core |
    | start-mode | 服务启动模式，用于init服务启动控制(包括：condition、boot、normal) |
    | jobs       | 当前服务在不同阶段可以执行的job"jobs" : {  "on-boot" : "services:console""on-start" : "services:console""on-stop" : "services:console"} |
    | ondemand   | 按需启动的服务需要配置的属性，配置有该属性的服务不会被start命令拉起；如果该服务配置有socket，init将会在解析服务时创建该socket并进行监听，当socket有消息上报时，init拉起对应服务 |

  - 模版

    ```json
    {
        "import" : [
                "/etc/example1.cfg",
                "/etc/example2.cfg"
        ],
        "jobs" : [{
                "name" : "jobName1",
                "cmds" : [
                    "start serviceName",
                    "mkdir dir1"
                ]
            }, {
                "name" : "jobName2",
                "cmds" : [
                    "chmod 0755 dir1",
                    "chown root root dir1"
                ]
            }
        ],
        "services" : [{
                "name" : "serviceName",
                "path" : ["/system/bin/serviceName"]
            }
        ]
    }
    ```

# *.para/ *.para.dac

- *.para: 系统参数值定义文件后缀名为".para"；如：const.product.name=OHOS-PRODUCT

- *.para.dac: 系统参数DAC访问控制定义文件，系统参数的访问权限控制通过自主访问控制(Discretionary Access Control)方式管理，访问权限定义文件后缀名为".para.dac" ；如：const.product.="root:root:660"

- 系统参数名称命名格式：点分格式，多端组成，每段由字母、数字、下划线组成，总长度不超过96字节
- 参数名称 

| 类别     | 名称                | 实例               | 说明                                      |
| -------- | ------------------- | ------------------ | ----------------------------------------- |
| 参数名称 | Parameter Name      | const.product.name | 完整的系统参数名称                        |
| 参数目录 | Parameter Directory | const.product.     | 以"."结尾，标志相同前缀的所有系统参数集合 |

- 参数类型

  > [ const | persist ].sub_system.desc
  >
  > sub_system：子系统或模块的名称
  >
  > desc：子系统或模块下参数的描述字符，为点分格式进行分级描述

| 类别     | 前缀     | 说明                                                         |
| -------- | -------- | ------------------------------------------------------------ |
| 常量     | const.   | 常量参数，不可变更，值最大长度为4096字节（包括结束符）       |
| 可写     | 其他     | 可写参数，重启丢失，值最大长度96字节（包括结束符）           |
| 可持久化 | persist. | 可写并可持久化保存参数，重启不会丢失，值最大长度96字节（包括结束符） |

- 参数赋值

| 类别   | 示例                                    | 说明                           |
| ------ | --------------------------------------- | ------------------------------ |
| 字符串 | const.ohos.fullname=OpenHarmony-4.0.9.2 | 多行字符串需要通过引号扩起来。 |
| 数字   | const.ohos.apiversion=10                | 直接赋值                       |
| 布尔   | const.telephony.enable=false\|true      | 布尔型的可以为0,1,false,true。 |

# ueventd.config

- 文件路径：base\startup\init\ueventd\etc\ueventd.config

- 内容

  ```js
  <device name> <mode> <uid> <gid> <parameter>
  /dev/null 0666 0 0
  /dev/binder 0666 0 0
  /dev/watchdog 0660  watchdog watchdog
  /dev/tty 0666 0 0
  ```

- 作用：可以设置驱动挂载到/dev目录下的所有者及其权限，用于对硬件设备访问权限控制；在系统init进程启动过程中进行解析加载，读取配置文件设备节点信息保存并做对应的权限处理

- 字段说明

  | 字段        | 说明                                                         | 例子                                                         |
  | ----------- | ------------------------------------------------------------ | ------------------------------------------------------------ |
  | device name | 设备节点名称                                                 | /dev/tty                                                     |
  | mode        | 设备节点所给予的权限，遵循UGO模型，通过chown进行配置         | 0660                                                         |
  | uid         | 设备节点的进程id                                             | 0==root                                                      |
  | gid         | 设备节点的gid                                                | 0==root                                                      |
  | parameter   | 设备节点参数信息，作用类似于通知，如果设备创建了，会设置一个对应parameter，根据参数信息便于其他进程对该节点进行对应的操作 | /dev/null 0666 0 0  device_null<br />如在dev/null节点下操作：<br />当设备被创建，设置startup.uevent.device_null "added"；当设备被卸载，我们会设置startup.uevet.device_null "removed" |


# Fstab

- 文件内容

  ```
  # fstab file.
  #<src>      <mnt_point> <type>  <mnt_flags and options>   <fs_mgr_flags>
  /dev/block/platform/fe310000.sdhci/by-name/system    /usr       ext4     ro,barrier=1  wait,required
  /dev/block/platform/fe310000.sdhci/by-name/vendor    /vendor        ext4     ro,barrier=1  wait,required
  /dev/block/platform/fe310000.sdhci/by-name/sys-prod     /sys_prod        ext4     ro,barrier=1  wait
  /dev/block/platform/fe310000.sdhci/by-name/chip-prod     /chip_prod        ext4     ro,barrier=1  wait
  /dev/block/platform/fe310000.sdhci/by-name/userdata      /data       f2fs     discard,noatime,nosuid,nodev,fscrypt=2:aes-256-cts:aes-256-xts,usrquota  wait,check,fileencryption=software,quota
  ```

- 字段说明

  | 字段                  | 说明                                                         |
  | --------------------- | ------------------------------------------------------------ |
  | src                   | 挂在分区的地址或文件系统的设备的路径                         |
  | mnt_point             | 在根文件系统中的挂载点                                       |
  | type                  | 文件系统的类型(常见有ext2、vfat、NTFS等)                     |
  | mnt_flags and options | 挂载的参数,详细的如下说明                                    |
  | fs_mgr_flags          | 文化系统管理的标识(Android10引入)<br />可用值有：{"check","wait,"required","nofail","hvb"} |

- mnt_flags and options 参数说明

  ```
  auto 在启动时或键入mount -a命令时自动挂载
  noauto 开机不自动挂载，不再使用mount　－a命令（例如系统启动时）加载该文件系统
  exec 允许执行此分区的二进制文件
  noexec 不允许执行此文件系统上的二进制文件
  ro 以只读模式挂载文件系统
  rw 以读写模式挂载文件系统
  user 允许任意用户挂载此文件系统，若无显示定义，隐含启用noexec nosuid nodev参数
  users 允许所有users组中的用户挂载文件系统
  nouser 只能被root挂载
  owner 允许设备所有者挂载
  sync 实时同步内存和磁盘中的数据，不对该设备的写操作进行缓冲处理，这可以防止在非正常关机时情况下破坏文件系统，但是却降低了计算机速度
  async 磁盘和内存不同步，系统每隔一段时间就会把内存数据写入磁盘中
  dev 解析文件系统上的块特殊设备
  nodev 不解析文件系统上的块特殊设备
  suid/nosuid 表示允许/不允许分区有suid属性
  quota　　　强制在该文件系统上进行磁盘定额限制
  usrquota 表示启动用户的磁盘配额模式。磁盘配额会针对用户限定他们使用的磁盘额度
  grpquota 表示启动组的磁盘配额模式
  nodiratime 不更新文件系统上的目录inode访问记录
  relatime 实时更新inode access记录
  defaults 使用文件系统的默认挂载参数，ext4的默认参数为rw,suid,dev,exec,auto,nouser,async
  ```


# sandbox.json

- 文件路径：\base\startup\init\services\sandbox

- 作用: 用于支持系统组件及芯片组件进程沙盒运行的配置文件，在init里面创建系统组件沙盒和芯片组件沙盒，native服务根据功能进入system沙盒或者chipset沙盒。在system-sandbox.json、chipset-sandbox.json等配置文件中设置沙盒组件中mount bind 的目录或文件，实现沙盒组件通过mount属性进行隔离。同时，提供了一种沙盒调试工具begetctl，当需要在沙盒内验证或者进行沙盒相关开发时，方便对需求进行调试、验证、完善。

-  配置文件分类说明

  | 配置文件               | 说明                       |
  | ---------------------- | -------------------------- |
  | chipset-sandbox64.json | 64位系统的芯片沙盒配置文件 |
  | chipset-sandbox.json   | 32位系统的芯片沙盒配置文件 |
  | system-sandbox64.json  | 64位系统的系统沙盒配置文件 |
  | system-sandbox.json    | 32位系统的系统沙盒配置文件 |

- 配置文件字段说明

  | JSON前缀         | 解释                                                |
  | ---------------- | --------------------------------------------------- |
  | sandbox-root     | 沙盒的根目录                                        |
  | mount-bind-paths | mount一个目录                                       |
  | mount-bind-files | mount一个文件                                       |
  | src-path         | 需要mount的目录/文件路径                            |
  | sandbox-path     | 沙盒里面需要挂载至的目录/文件                       |
  | sandbox-flags    | mount的挂载标志位, 缺省"bind rec"标志位             |
  | ignore           | 是否忽略mount失败，设置为1 则忽略失败，继续往下执行 |
  | target-name      | 需要link的目录                                      |
  | link-name        | 沙盒内link后的目录                                  |

- 修改方案

  - 查看系统组件沙盒配置文件、芯片组件沙盒配置文件，进入/system/etc/sandbox/ 目录下，cat system-sandbox.json ，cat chipset-sandbox.json; 
  - 直接修改对应沙盒配置文件， 重新启动。 
  - 对于64位系统，cat system-sandbox64.json ，cat chipset-sandbox64.json。