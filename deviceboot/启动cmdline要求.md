# 设备启动

## 概述

OpenHarmony init启动时需要加载一些硬件参数，这些参数由bootloader通过cmdline传递给内核，init通过解析/proc/cmdline来获取。

### 基本格式

默认cmdline采用name=value格式。

### hardware



| 名称                           | 对应参数           | 作用                                                         | 默认值 |
| ------------------------------ | ------------------ | ------------------------------------------------------------ | ------ |
| hardware                       | ohos.boot.hardware | 单板级差异都可以通过ohos.boot.hardware来区分，例如挂载文件系统时通过/vendor/etc/init.${ohos.boot.hardware}.cfg来动态选择 | 无     |
| default_boot_device            | 无                 | 用于创建/dev/block/by-name，该目录指向实际的启动存储设备。例如：default_boot_device=soc/10100000.himci.eMMC | 无     |
| sn                             | ohos.boot.sn       | 用于获取设备的唯一序列号，bootloader通过cmdline名称ohos.boot.sn来传递SN号。 | 无     |
| ohos.required_mount.{partname} | 无                 | 提供必要的挂载分区信息。init在启动的后，会解析/proc/cmdline中ohos.required_mount.{partname}字段，获取必要的分区，并将其挂载好。格式举例如下： ohos.required_mount.system=/dev/block/by-name/system@/usr@ext4@ro,barrier=1@wait,required 这个例子中，表示system分区是必要的分区，需要init提前挂载。各字段通过符号"@"分割。分别表示{block device}@{mount point}@{fstype}@{mount options}@{fstab flags}。 fstab flags的定义请参考代码：base/startup/init_lite/interfaces/innerkits/include/fs_manager/fs_manager.h。 | 无     |



### 系统参数权限基本要求



### 系统参数定义方法

